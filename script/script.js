$('#header__icon').click(function(e){
	e.preventDefault();
	$('body').addClass('with--sidebar');
	$('body').addClass('nav-open');
});

$('#site-cache').click(function(e){
  $('body').removeClass('with--sidebar');
  $('body').removeClass('nav-open');
});

$('#menu-links').click(function(e){
	$('body').removeClass('with--sidebar');
	$('body').removeClass('nav-open');
});

document.onscroll = function() {
	const pos = showPosition(document.body);
	if (pos > 10) {
		$('#back-to-top').css('display', 'block');
	}

	if (pos < 10) {
		$('#back-to-top').css('display', 'none');
	}
}

function showPosition(elm){
		const parent = elm.parentNode,
			pos = (elm.scrollTop || parent.scrollTop) / (parent.scrollHeight - parent.clientHeight ) * 100;
		
		return pos;   
}

$("#form").submit(function (event) {
	event.preventDefault();
	validateFields();
});

function validateFields() {
	const emailReg = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
	const phoneReg = /^[7-9][0-9]{9}$/;
	const name = $("#name").val();
	const email = $("#email").val();
	const phone = $("#phone").val();
	const message = $("#message").val();
	let errors = false;

	clearFields();

	if(name == undefined || name == "") {
		$('#name_div p').addClass('is-invalid')
		errors = true
	}

	if(email == undefined || email == "") {
		$('#email_div .empty').addClass('is-invalid');
		errors = true;
	} else if(!email.match(emailReg)) {
			$('#email_div .invalid').addClass('is-invalid');
				errors = true;
	}

	if(phone == undefined || phone == "") {
		$('#phone_div .empty').addClass('is-invalid');
		errors = true;
	} else if(!phone.match(phoneReg)) {
			$('#phone_div .invalid').addClass('is-invalid');
				errors = true;
	}

	if(message == undefined || message == "") {
		$('#message_div p').addClass('is-invalid')
		errors = true
	}

	if(errors) return;
	submitForm(name, email, phone, message);
}

function clearFields() {
		$(`#phone_div .empty,
			 #phone_div .invalid,
			 #email_div .empty,
			 #email_div .invalid,
			 #name_div p,
			 #message_div p`).removeClass('is-invalid');
}

function submitForm(name, email, phone, message) {
	$.ajax({
			type: "POST",
			url: "mail.php",
			data: "name=" + name + "&email=" + email + "&phone=" + phone + "&message=" + message,
			success() {
				$('#form').trigger("reset");
				$('.success').addClass('show-alert');

				setTimeout(() => {
					$('.success').removeClass('show-alert');						
				}, 3000);
			},
			error() {
				$('.fail').addClass('show-alert');

				setTimeout(() => {
					$('.fail').removeClass('show-alert');						
				}, 3000);
			}
	})
}